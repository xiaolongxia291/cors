from flask import Flask,jsonify
from flask_cors import CORS


app = Flask(__name__)

# 配置CORS：包含了后端路由、前端的ip和端口
CORS(app, resources={r"/api/hello": {"origins": "http://192.168.2.107:8080"}})

# @app.route('/api/hello', methods=['GET'])
# def get_data():
#     return jsonify(data='hello,flask!')

if __name__ == '__main__':
    app.run(host='192.168.2.107', port=2020)
